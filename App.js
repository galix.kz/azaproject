import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, TouchableOpacity } from 'react-native';
import { BleManager } from 'react-native-ble-plx';
import {check, PERMISSIONS, request, RESULTS} from "react-native-permissions";
import BluetoothSerial, {withSubscription} from "react-native-bluetooth-serial-next";
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';


class App extends React.Component{
  constructor(props) {
    super(props);
    this.manager = new BleManager();
    this.state = {
      temp:"asd asd ",
      start: false,
      data: {

      }

    }
  }

    async start()
    {
      try {
        const granted = await request(
            PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION, {
              title: 'Location permission for bluetooth scanning',
              message: 'wahtever',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
        );
        if (granted === RESULTS.GRANTED) {
          console.log('Location permission for bluetooth scanning granted');
          await BluetoothSerial.writeToDevice("This is the test message");
          // const data = await BluetoothSerial.readUntilDelimiter("$");
          // console.log(data)
          BluetoothSerial.read((data, subscription) => {

            // if (this.imBoredNow && subscription) {
            //   BluetoothSerial.removeSubscription(subscription);
            // }
          }, "\r\n");
          const devices = await BluetoothSerial.list();
          // console.log(devices)
          let device = devices.filter(word => word.name === "AMIREK");
          if (device.length > 0) {
            device = device[0]
            const myDevice = BluetoothSerial.device(device.id);
            await myDevice.connect()
            await myDevice.write('This is a message for my mydevice.');
            console.log(device.id)
            let yourReadSubscription;
            let that = this;
            try{
              await BluetoothSerial.readEvery(
                  (data, intervalId) => {
                    let d = that.state.data;
                    console.log(data);
                    let x = data.split('').map(function (char) {
                      console.log(char.charCodeAt(0));
                      let y = char.charCodeAt(0).toString(16).toUpperCase();
                      return y.length == 1 ? "0"+y : y;
                     }).join(' ');
                     let temp = that.state.temp;
                     temp += x;
                     let i = temp.indexOf('CC FF FF');
                     if (i != -1) temp = temp.substring(i);
                     while (temp.indexOf("CC FF FF", temp.indexOf("CC FF FF") + 1) != -1 ) {
                       let z = temp.substring(0, temp.indexOf("CC FF FF", temp.indexOf("CC FF FF") + 1));
                       console.log('FOUND', z);
                       d[z.split(' ').join('').trim()] = new Date();
                       temp = temp.substring(temp.indexOf("CC FF FF", temp.indexOf("CC FF FF") + 1));
                      }
                      if (temp.length == 83) {
                        console.log('*****2', temp);
                        d[temp.split(' ').join('').trim()] = new Date();
                        temp = "";
                      } 
                    console.log("TEMP", temp)
                    that.setState({temp: temp})
                    that.setState({data: d})
                    // CC FF FF 10 32 15 01 E2 00 00 17 23 0B 00 23 15 50 77 1E 01 3A FC 00 0A 8F E8 90 52 
                    // CC FF FF 10 32 15 01 E2 00 00 17 23 0B 00 23 15 50 77 1E 01 3A FC 00 0A 8F E8 90 52
                  },
                  1000,
                  null
              );
            }
            catch (e) {
              // console.log('error', e)
            }

          }
        } else {
          console.log('Location permission for bluetooth scanning revoked');

        }
      } catch (err) {
        // console.warn(err);

      }
    }

    render()
    {
      return (
          <SafeAreaProvider>
            <SafeAreaView style={{ flex: 1  }}>
              {this.state.start ? (
                      <ScrollView style={[styles.container]}>
                        <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize:60, marginBottom: 15, color: 'black'}}>{Object.keys(this.state.data).length}</Text>
                        </View>
                        {Object.entries(this.state.data).map(data=>{
                          return (
                              <View style={styles.button} key={data[0]}>
                                <Text style={{fontSize: 20}}>{data[0]}</Text>
                              </View>
                          )
                        })}

                      </ScrollView>
              ) :
                  (
                     <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                       <TouchableOpacity   style={{padding: 30, backgroundColor: 'blue'}} onPress={()=>{
                         this.setState({start: true})
                         this.start()
                        // console.log('asjdkajsklj')
                       }}>
                         <Text style={{ color: "#FFFFFF", fontSize: 30 }}>start</Text>
                       </TouchableOpacity>
                     </View>
                  )
              }

              <StatusBar style="auto"/>

            </SafeAreaView>
          </SafeAreaProvider>
      );
    }


  }
export default withSubscription({ subscriptionName: "events" })(App)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 15
  },
  button: {
    borderWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 5,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderColor: '#979797'
  }
});
